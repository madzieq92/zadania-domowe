package com.company.kolekcje_listy.zadanie1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> lista = new ArrayList<>();
        System.out.println("Podaj 5 liczb: ");

        for (int i = 0; i<5; i++) {
            lista.add(scanner.nextInt());
        }

        System.out.println("Podane liczby: " + lista);
    }
}