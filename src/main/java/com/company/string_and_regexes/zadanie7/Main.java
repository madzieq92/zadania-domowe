package com.company.string_and_regexes.zadanie7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz tekst: ");
        String text = scanner.nextLine();

        //podpunkt a)
        System.out.println("\nTekst po zmianie na wielkie litery:");
        System.out.println(text.toUpperCase());

        //podpunkt b)
        System.out.println("\nTekst po zmianie na małe litery:");
        System.out.println(text.toLowerCase());
    }
}
