package com.company.string_and_regexes.zadanie8;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<String, Integer> hashMapa = new HashMap<String, Integer>();

        System.out.println("Wpisz tekst: ");
        String tekst = scanner.nextLine();

        System.out.println("\nIlość wystąpień poszczególnych słów w tekście:");
        System.out.println(zliczWystapieniaSlow(tekst));
    }

    public static HashMap<String, Integer> zliczWystapieniaSlow(String tekst) {
        tekst = tekst.replaceAll("[^a-zA-Z_0-9_\\s]", "");      //usuwa wszustkie znaki typu ,".- z tekstu
        String[] stringArray = tekst.split("\\s");
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        int counter = 0;

        for (int i = 0; i < stringArray.length; i++) {
            if (hashMap.containsKey(stringArray[i]) == false) {
                hashMap.put(stringArray[i], 1);
            } else {
                hashMap.replace(stringArray[i], hashMap.get(stringArray[i]) + 1);
            }
        }
        return hashMap;
    }
}
