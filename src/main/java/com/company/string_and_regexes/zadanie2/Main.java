package com.company.string_and_regexes.zadanie2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz tekst: ");
        String text = scanner.nextLine();

        //podpunkt a)
        if (text.contains("ania")) {
            System.out.println("Słowo ania znajduje się w podanym tekście.");
        } else {
            System.out.println("Słowa ania nie ma w podanym tekście.");
        }

        //podpunkt b)
        if (text.startsWith("ania")) {
            System.out.println("Podany tekst zaczyna się od słowa ania.");
        } else {
            System.out.println("Podany tekst nie zaczyna się od słowa ania.");
        }

        //podpunkt c)
        if (text.endsWith("ania")) {
            System.out.println("Podany tekst kończy się słowem ania.");
        } else {
            System.out.println("Podany tekst nie kończy się słowem ania.");
        }

        //podpunkt d)
        if (text.equals("ania")) {
            System.out.println("Podany tekst = ania.");
        } else {
            System.out.println("Podany tekst jest inny niż ania.");
        }

        //podpunkt e)
        if (text.toLowerCase().contains("ania")) {
            System.out.println("Słowo ania (bez względu na wielkość liter) znajduje się w podanym tekście.");
        } else {
            System.out.println("Słowa ania (bez względu na wielkość liter) nie ma w podanym tekście.");
        }

        //podpunkt f)
        int index = text.indexOf("ania");
        if (index != -1) {
            System.out.println("Index pierwszego wystąpienia ania to: " + index + ".");
        } else {
            System.out.println("W podanym tekście nie ma słowa ania.");
        }
    }
}
