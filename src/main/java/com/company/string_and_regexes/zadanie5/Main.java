package com.company.string_and_regexes.zadanie5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz tekst: ");
        String text = scanner.nextLine();

        int[] tabCount = new int[1000];  //stores count of each character; index of tab = ASCII of char

        for (int i = 0; i < text.length(); i++) {
            tabCount[text.charAt(i)]++;
        }

        for (int i = 0; i < tabCount.length; i++) {
            if (tabCount[i] != 0) {
                System.out.println("Znak '" + (char) (i) + "' występuje " + (int) tabCount[i] + " raz(y).");
            }
        }
    }
}
