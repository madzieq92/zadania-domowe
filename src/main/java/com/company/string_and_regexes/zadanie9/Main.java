package com.company.string_and_regexes.zadanie9;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner zmiennaTypuScanner = new Scanner(System.in);
        char symbolOperacji;
        double[] tablica = new double[2];

        System.out.println("Podaj pierwszą liczbę: ");
        tablica[0] = zmiennaTypuScanner.nextDouble();

        symbolOperacji = getCharacter();

        System.out.println("Podaj drugą liczbę: ");
        tablica[1] = zmiennaTypuScanner.nextDouble();

        while (tablica[1] == 0 && symbolOperacji == '/') {
            System.out.println("Twój dzielnik jest równy 0. Podaj drugą liczbę jeszcze raz.");
            tablica[1] = zmiennaTypuScanner.nextDouble();
        }

        calculate(symbolOperacji, tablica[0], tablica[1]);
    }

    public static char getCharacter() {
        Scanner zmiennaTypuScanner = new Scanner(System.in);
        System.out.println("Podaj rodzaj działania : *,+,-,/");

        return zmiennaTypuScanner.next().charAt(0);
    }

    public static void wykonajMnozenie(double n1, double n2) {
        System.out.println(n1*n2);
    }

    public static void wykonajDodawanie(double n1, double n2) {
        System.out.println(n1+n2);
    }

    public static void wykonajOdejmowanie(double n1, double n2) {
        System.out.println(n1-n2);
    }

    public static void wykonajDzielenie(double n1, double n2) {
        System.out.println(n1/n2);
    }

    public static void calculate(char znak2, double n1, double n2) {
        System.out.println("Otrzymany wynik wynosi: ");

        switch(znak2) {
            case '*':
                wykonajMnozenie(n1, n2);
                break;
            case '+':
                wykonajDodawanie(n1, n2);
                break;
            case '-':
                wykonajOdejmowanie(n1, n2);
                break;
            case '/':
                wykonajDzielenie(n1, n2);
                break;
            default:
                System.out.println("Wybrano błędny symbol operacji matematycznej.");
        }
    }
}
