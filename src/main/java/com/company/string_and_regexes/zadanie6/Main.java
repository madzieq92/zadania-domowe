package com.company.string_and_regexes.zadanie6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz tekst: ");
        String text = scanner.nextLine();

        System.out.println("\nTekst po zmianie przecinków na słowo \"makarena\":");
        System.out.println(text.replaceAll(",", "macarena"));
    }
}
