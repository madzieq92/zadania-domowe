package com.company.string_and_regexes.zadanie3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz tekst: ");
        String text = scanner.nextLine();

        System.out.println("Wpisz słowo: ");
        String searched = scanner.nextLine();

        if (text.contains(searched)) {
            System.out.println("Słowo " + searched + " znajduje się w podanym tekście.");
        } else {
            System.out.println("Słowo " + searched + " nie znajduje się w podanym tekście.");
        }

    }
}
